export default (
    state = {
      itemGifDetail: {},
      isShowDialog: false
    },
    action
  ) => {
    switch (action.type) {
      case 'click_item':
        return {
          isShowDialog: true,
          itemGifDetail: action.payload
        };
      case 'dismiss_dialog':
        return {
          isShowDialog: false,
          itemGifDetail: {}
        };
      default:
        return state;
    }
  };
  