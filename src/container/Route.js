import React from 'react';
import { Route, Switch } from 'react-router-dom';
import LoginPage from './LoginPage';
import MainPage from './Main';
import RegisterPage from './RegisterPage';
//import ListFavorite from '../component/favorite/list';

function Routes() {
    return (
        <div style={{ width: '100%' }}>
            <Switch>

                <Route 
                exact path="/" 
                component={LoginPage} />
                 <Route exact path="/register" component={RegisterPage} />
                <Route 
                //  exact path="/giphy" 
                component={MainPage} />
            </Switch>
        </div>
    )
}

export default Routes;