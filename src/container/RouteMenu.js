import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import ListFavorite from '../component/favorite/list';
import ListGif from '../component/ListGif' 

// import Profile from '../Components/profile/index';

function RouteMenu(props) {
    return (
      <Switch>
        <Route
          path="/giphy"
          exact
          render={() => {
            return <ListGif items={props.items} />;
          }}
        />
        <Route path="/favorite" exact  component={ListFavorite}/>
        {/* <Redirect from="/*" exact to="/" /> */}
      </Switch>
    );


}
export default RouteMenu;