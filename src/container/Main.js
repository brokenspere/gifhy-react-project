import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Spin } from 'antd';
import { Modal, Button, Layout, Menu, message, Input, Row, Col,notification } from 'antd';
import { auth } from '../firebase'
import RouteMenu from './RouteMenu';
import { connect } from 'react-redux';
import { Pagination } from 'antd'

//const KEY_USER_DATA = 'user_data';

const { Header, Content, Footer } = Layout;
const menus = ['giphy', 'favorite'];
const Search = Input.Search;
const GphApiClient = require('giphy-js-sdk-core')
const client = GphApiClient("ofYVrivLKZFcLN0vsRbJz4P5i9y2YXxw")
const favStorageKey = 'list-fav'


const mapStateToProps = state => {
    return {
        isShowDialog: state.isShowDialog,
        itemGifClick: state.itemGifDetail
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onDismissDialog: () =>
            dispatch({
                type: 'dismiss_dialog'
            }),
        onItemGifClick: item =>
            dispatch({
                type: 'click_item',
                payload: item
            })
    };
};

class Main extends Component {
    state = {
        items: [],
        email: '',
        isShowDialog: false,
        isLoading: false,
        isShowModal: false,
        ItemGif: null,
        pathName: menus[0],
        favItems: []
    }
    onModalClickOk = () => {
        this.props.onDismissDialog();
    };
    onModalClickCancle = () => {
        this.props.onDismissDialog();
    };

    ///----------------------------------------------log out session---------------------------------------------------------------///
    showDialogConfirmLogout = () => {
        console.log('modal work')
        this.setState({ isShowDialog: true });
    };

    handleCancel = () => {
        console.log('cancle click work')
        this.setState({ isShowDialog: false });
    }

    handleOk = () => {
        console.log('logout work')
        this.setState({ isLoading: true });
        localStorage.setItem(
            'user-data',
            JSON.stringify({
                isLoggedIn: false
            })
        );
        setTimeout(() => {
            this.setState({ isLoading: false });
            this.props.history.push('/');
        }, 1000);

    }

  

    ///----------------------------------------------log out session---------------------------------------------------------------///


    loadDataApi(){
      
        fetch('http://api.giphy.com/v1/gifs/trending?&api_key=ofYVrivLKZFcLN0vsRbJz4P5i9y2YXxw&limit=400')
            .then(response => response.json())
            .then(items => this.setState({ items: items.data })); // change result to data

    }

    componentDidMount() {
        const jsonStr = localStorage.getItem(favStorageKey);
        const items = JSON.parse(jsonStr) || [];
        this.setState({ favItems: items });

        const { pathname } = this.props.location;
        var pathName = menus[0];
        if (pathname != '/') {
            pathName = pathname.replace('/', '');
            if (!menus.includes(pathName)) pathName = menus[0];
        }
        this.setState({ pathName });

            this.loadDataApi()

    }

    onMenuClick = e => { // change path
        var path = '/';
        if (e.key != 'home') {
            path = `/${e.key}`;
            this.loadDataApi()
        }
        this.props.history.replace(path);
    };

    onClickFavorite = () => { // save to favorite
        // TODO: save item to localstorage
        const itemClick = this.props.itemGifClick;
        const items = this.state.favItems;

        const result = items.findIndex(item => {
            return item.title === itemClick.title;
        });
        console.log('items', items)
        console.log('itemClick', itemClick)
        console.log('result', result)
        if(result != -1){
            items.splice(result,1);
            localStorage.setItem(favStorageKey, JSON.stringify(items));
            message.success('Unfavorite this item successfully', 1, () => {
                this.setState({ favItems: items });
                this.onModalClickCancle();
              });
        
        } else {
            items.push(itemClick);
            localStorage.setItem(favStorageKey,JSON.stringify(items));

            message.success('Saved to favorite', 1);
            this.setState({favItems:items})
            this.onModalClickCancle();
        }

    };

    checkItemFavorited = () => {
        const items = this.state.favItems;
        const itemGif = this.props.itemGifClick;
        const result = items.find(item => {
          return item.title === itemGif.title;
        });
        if (result) {
          return 'primary';
        } else {
          return '';
        }
      };


    onClickCopyToClipboard = () => {

        const item = this.props.itemGifClick;
        navigator.clipboard.writeText(item.images.fixed_width.url)
        message.success('Copied to clipboard', 1);
    }

    onSearchGif = (value) => {
        console.log(value)
        console.log('search called')
        if (value.trim().length === 0) {
            value = ''
            this.loadDataApi()
            return
        }
        if (value != '' && value != null && value != {}) {
            client.search('gifs', { "q": value ,"limit":100})
                .then((response) => {
                    // 
                    if(response.data.length !==0){
                        console.log( response.data)
                        this.setState({ items: response.data })
                    }else{
                        message.error('No gif found',1)
                        this.loadDataApi()
                    }
                    
                })
                .catch(() => {

                })
        } else {
            this.setState({ items: this.state.items })
        }

    }


    render() {

        const item = this.props.itemGifClick;
        console.log(this.state.items)

        return (

            <div>

                {this.state.items.length > 0 ? (
                    <div >
                        {''}

                        <Layout className="layout" style={{ background: 'black' }}>

                            <Header
                                style={{
                                    padding: '0px',
                                    position: 'fixed',
                                    zIndex: 1,
                                    width: '100%'
                                }}

                            >


                                <Row type="flex" justify="space-around" align="middle" >

                                     <h2 className="head">GIPHY</h2>   
                                   
                                    <Col span={9} push={5}>
                                        <Search placeholder="Search gif here" style={{ marginTop: 15 }} onSearch={this.onSearchGif} enterButton />
                                    </Col>
                                    
                                    <Col span={7} push={4}>
                                        <Menu className="Menu"
                                            theme="dark"
                                            mode="horizontal"
                                            defaultSelectedKeys={[this.state.pathName]}
                                            style={{ lineHeight: '53px' }}
                                            onClick={e => {
                                                this.onMenuClick(e);
                                            }}
                                        >

                                            <Menu.Item key={menus[0]}>Home</Menu.Item>
                                            <Menu.Item key={menus[1]}>Favorite</Menu.Item>
                                        </Menu>
                                    </Col>

                                    <Col  span={3} push={0.5}>
                                        <Button type="danger" onClick={this.showDialogConfirmLogout}>Logout</Button>
                                    </Col>

                                </Row>



                            </Header>

                            <Content
                                style={{
                                    padding: '16px',
                                    marginTop: 64,
                                    minHeight: '600px',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    display: 'flex',
                                    background: 'black'

                                }}
                            >
                                <RouteMenu items={this.state.items} />

                            </Content>
                            <Footer style={{ textAlign: 'center', color: 'white', background: 'black' }}>
                                Giphy-project
                             </Footer>


                        </Layout>
                        <Modal
                            title="Logout"
                            visible={this.state.isShowDialog}
                            onOk={this.handleOk}
                            onCancel={this.handleCancel}
                        >
                            <b>Are you sure?</b>
                        </Modal>
                    </div>
                ) : (
                        <Spin size="large" />
                    )}
                {item != null ? (
                    <Modal
                        width="40%"
                        style={{ maxHeight: '50%' }}
                        title={item.title}
                        visible={this.props.isShowDialog}
                        onCancel={this.onModalClickCancle}
                        footer={[
                            <Button
                                key="favorite"
                                type="primary"
                                icon="like"
                                size="large"
                                shape="circle"
                                onClick={this.onClickFavorite}
                            />,

                            <Button
                                key="submit"
                                type="primary"
                                icon="link"
                                size="large"
                                shape="circle"
                                onClick={this.onClickCopyToClipboard}
                            />
                        ]}
                    >
                        {item.images != null ? (
                            <img src={item.images.fixed_width.url} style={{ width: '100%' }} /> //change 
                        ) : (
                                <div></div>
                            )}
                        {/* */}
                        <br />
                        <br />

                    </Modal>
                ) : (
                        <div />

                    )}

            </div>

        )

    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Main);