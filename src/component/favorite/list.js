import React, { Component } from 'react';
import { List } from 'antd';
import ItemFavorite from './item';
const favStorageKey = 'list-fav'

class ListFavorite extends Component {
    state = {
        items: []
    }
    componentDidMount() {
        const jsonStr = localStorage.getItem(favStorageKey);
        if (jsonStr) {
            const items = JSON.parse(jsonStr);
            console.log('fav', items);
            this.setState({ items });
        }
    }
    render() {
        return (
            <List style ={{width:'100%'}}
                pagination={{
      
                pageSize: 40,
              }}
                //   grid={{ gutter: 16, column: 4 }}
                dataSource={this.state.items}
                grid={{ gutter: 16, column: 4 }}
                renderItem={item => (
                    <List.Item>
                        <ItemFavorite item={item} onItemGifClick={this.props.onItemGifClick} />
                    </List.Item>
                )}
            />
        )
    }
}
export default ListFavorite;