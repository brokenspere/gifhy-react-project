import React ,{Component} from 'react';
import { Card } from 'antd';
import { connect } from 'react-redux';
const { Meta } = Card;

const mapDispatchToProps = dispatch => {
    return {
      onItemGifClick: item =>
        dispatch({
          type: 'click_item',
          payload: item
        })
    };
  };


  class ItemFavorite extends Component{
      render(){
        const item = this.props.item;
          return(
            <Card
            onClick={() => {
              this.props.onItemGifClick(item);
            }}
             
            hoverable
            cover={<img alt="example" src={item.images.fixed_width.url} 
            style={{ height: 300 }}
            />}
          >
            <Meta
              title={item.title}
            />
          </Card>
          )
      }
  }
  export default connect(
    null,
    mapDispatchToProps
)(ItemFavorite);