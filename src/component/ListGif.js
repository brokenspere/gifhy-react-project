import React from 'react';
import ItemGif from './itemGif';
import { List } from 'antd';

function ListGif(props) {
  console.log('items ', props.items);

  return (
    <List
    pagination={{
      
      pageSize: 40,
    }}
    //   grid={{ gutter: 16, column: 4 }}
      dataSource={props.items}
      grid={{ gutter: 16, column: 4 }}
      renderItem={item => (
        <List.Item>
          <ItemGif item={item} onItemGifClick={props.onItemGifClick} />
        </List.Item>
      )}
    />
  );
}

export default ListGif;
