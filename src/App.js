import React, { Component } from 'react';
import 'antd/dist/antd.css';
import './App.css';

import Routes from './container/Route'

class App extends Component {
  render() {
    return (
      <div className="App">
       <Routes/>
      </div>
    );
  }
}

export default App;
